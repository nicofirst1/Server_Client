import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by nicolo on 23/04/2017.
 */
public class Client implements Runnable{

    //indirizzo ip del local host e porta alla quale connettersi
    private final int port=3000;
    public final String ip="127.0.0.1";

    //compoenenti per la comunicazione
    private Socket s;
    private ObjectInputStream in;
    private ObjectOutputStream out;

    public int id;
    public volatile boolean running;//volatile perche usata da piu thread
    public Client_GUI main;

    public final Logger logger=Logger.getLogger("client.logger");




    public Client(Client_GUI main)
    {
        this.main=main;

        try{
            //inizzializzo la connessione, da notare che subito dopo l'inizzializzazione del socket, creo out
            // se creassi prima la pipe si bloccherebbe
            //inoltre ricordate che l'inizzializzazione deve essere simmetrica, ovvero client e server devono
            // inizzializzare out/in nello stesso ordine
            this.s=new Socket(ip,port);
            this.out=new ObjectOutputStream(s.getOutputStream());
            this.in=new ObjectInputStream(s.getInputStream());
            logger.log(Level.INFO,"comunication setup completed");

        }catch (IOException e){
            logger.log(Level.WARNING,"Cannot initialize client",e);
            System.exit(0);

        }
    }


    //la funzione handshake è ricorrente nel progetto, serve per vedere
    // se il client e il server riescono a comunicare
    public void handshake()
    {
        boolean correct=false;
        int resp=-1;

        do {
            try {
                //leggo l'id
                resp=in.readInt();

                //invio quello che ho letto
                out.writeInt(resp);
                out.flush();//ricardate di fare sempre il flush dopo un write altrimenti si blocca tutto

                //vedo se è corretto
                correct=in.readBoolean();

            } catch (IOException e) {
                e.printStackTrace();
                main.write_to_client("handshake failed",main.server_msg);
                logger.log(Level.WARNING,"handshake failed",e);
                running=false;
            }


        }while (!correct);//ripeto finche la comunicazione non è corretta

        id=resp;
        logger.log(Level.INFO,"connected with id: "+id);

    }

    //la funzione comand serve per smistare i vari comandi ricevuti dal server
    public void comand(Object resp_obj)
    {
        //se la risposta è una stringa
        if(String.class.isInstance(resp_obj))
        {
            String resp_str=(String)resp_obj; //casto la risposta

            //la scrivo sull'interfaccia gui
            main.write_to_client(resp_str,main.server_msg);

            //se contiene la parola goodbye interrompo la connessione
            if(resp_str.toLowerCase().contains("goodbye"))
            {
                kill();
            }
        }
        //altrimendi è un intero che rappresenta un comando
        else if(Integer.class.isInstance(resp_obj))
        {

                try {
                    //casto la risposta ad intero
                    int resp_int=(int) resp_obj;

                    switch (resp_int)
                    {
                        //se è uno allora devo scaricare un file
                        case 1:
                        {
                            //aspetto che il server inizzializzi il suo serversocket
                            in.readObject();
                            logger.log(Level.INFO,"creating new file reader");
                            //creo un nuovo thread per scaricare il file
                            new Thread(new File_Downloader_GUI(this)).start();
                        }
                        //se è due allora devo inviare un file e inizzializzo un nuovo thread
                        case 2:
                        {
                            logger.log(Level.INFO,"creating new file writer");
                            new Thread(new File_Writer_GUI(this)).start();
                        }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }


        }

    }


    //la funzioen kill è ricorrente in tutto il programma e verrà spiegata solamente qui dato che la metodologia è
    // simile in ogni implementazione, la sua funzione è quella di chiudere la connessione "gentilemnte"
    public int kill()
    {

        try{
            //chiudo prima gli streamers
            this.out.close();
            this.in.close();
            //poi il socker
            this.s.close();
            logger.log(Level.INFO,"connection closed");
            //notifico l'utente dell'avvenuta chiusura della connessione
            JOptionPane.showMessageDialog(null,"Connection closed");
            //distruggo la finestra main
            main.dispose();
            running=false;
            return 1;
        }catch (IOException e){
            logger.log(Level.WARNING,"cannot close connection ",e);
            running=false;
            return 0;
        }


    }


    //Comunication
    //è importante usare l'idioma synchronized poiche questo metodo è chiamato da piu thread
    // la funzione prende in ingresso un'interfaccia di tipo I (descritta sotto) e applica
    // alla risposta una funzione di mia scelta che usa come wrapper l'interfaccia I
    public synchronized void listen_from_server(I function)
    {
        //leggo la risposta
        Object resp_obj;

        try {
            resp_obj = in.readObject();
            //ci deve essere un qualche tipo di sincronizzazione tra invio e ricezzione di messaggi
            // tra client e server, io ho optato per l'approccio in cui la funzione che si occupa di ascoltare i
            // messaggi in entrata invia un acknoledge dell'avvenuta ricezione del messaggio, mentre la funzione che
            // invia i messaggi aspetta l'ack prima di continuare la sua esecuzione. Questo approccio è simmetrico,
            // quindi implementato cosi come descritto sia nel Server che nel Client
            out.writeBoolean(true);
            out.flush();

            //applico la funzione
            function.method(resp_obj);



        } catch (EOFException |SocketException e) {
            //questa eccezzione indica che il server ha chiuso la connessione, quindi notifico l'utente e chiuso il socket
            main.write_to_client("server has closed connection",main.system_msg);
            kill();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    //la funzione di write ha la metodologia descritta in precendeza, prende in input un oggetto da inviare al server
    // e un boolean che determina se dopo l'invio del messaggio devo mettermi in modalità di ricezione di una risposta,
    // normalmente è true, ma in alcuni casi (come l'invio/ricezione di un file) non devo attendere la risposta del server
    public synchronized void write_to_server(Object msg, boolean listen_too)
    {

        try {

                //invio l'oggetto e eseguo il flush della pipe
                out.writeObject(msg);
                out.flush();

                //attendo la ricezione dell'ack, e verifico che il messaggio sia stato ricevuto correttamente,
                //altrimenti notifico, l'utente
                if(!in.readBoolean())
                {
                    logger.log(Level.WARNING,"error reading");
                    main.write_to_client("Comand not recived correctly",main.system_msg);

                }


        } catch (EOFException e) {
            main.write_to_client("Server has closed connection",main.system_msg);
            kill();
        } catch (IOException e) {
            e.printStackTrace();
            logger.log(Level.WARNING,"cannot write message: "+msg,e);

        }
        //se devo anche ricevere la risposta del server chiamo il metodo listen con funzione di ingresso comand
        if(listen_too) listen_from_server(resp -> comand(resp));
    }



    //interfaccia che fa da wrapper per la funzione che volgio applicare
    interface I{
        void method(Object obj);
    }




    @Override
    public void run() {
        //imposto il running a tru
        running=true;

        //eseguo l'handshake
        handshake();

        //scrivo il messaggio di benvenuto sull'interfaccia grafica
        String msg="Command format is: \n[Who has to execute it]:" +
                "[what command]:[optional arguments]\n" +
                "'main' is the primary server, try 'main:help'\n" +
                "Tip: to navigate through your commands use arrow keys";
        main.write_to_client(msg, main.server_msg);




    }
}

