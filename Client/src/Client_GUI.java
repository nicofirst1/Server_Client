import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

/**
 * Created by nicolo on 23/04/2017.
 */
public class Client_GUI extends JFrame {


    public JTextField user_in;
    public JTextPane text_area;
    public StyledDocument sd;
    public JScrollPane scroll;
    public JScrollBar vertical;


    public SimpleAttributeSet server_msg;
    public SimpleAttributeSet client_msg;
    public SimpleAttributeSet system_msg;

    public List<String> all_cms;//lista di comandi digitati dall'utente
    public int cmd_idx;

    public Client ct;


    public Client_GUI()
    {
        this.ct=new Client(this);

        //inizzializzo i componenti principali
        user_in=new JTextField();
        text_area=new JTextPane();
        sd=text_area.getStyledDocument();
        scroll=new JScrollPane(text_area);
        all_cms=new ArrayList<>();

        //faccio in modo che la view dello scroll sia sempre in fondo
        vertical=scroll.getVerticalScrollBar();

        //inizzializzo la grafica per i messaggi
        style_setup();


        //non si scrive qui
        text_area.setEnabled(false);


        //aggiungo un listener per il tasto enter, metodo ovverride, lo uso solamente a scopi didattici,
        // poi verrà sostituito con il metodo complementare lambda disponibile con java 8+
        user_in.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //prendo il testo rilevato dal'enter
                String text=user_in.getText();
                //aggiungo il comando alla lista aggiornando l'indice
                all_cms.add(text);
                cmd_idx=all_cms.size();

                // scrivo il comando sullo schermo
                write_to_client(text,client_msg);
                //invio il comando al server
                ct.write_to_server(text,true);
                //elimino il comando dalla riga di input
                user_in.setText("");

            }});

        //aggiungo un listener per le freccette, cosi ottengo un comportamento simile al terminale dove la pressione
        // delle freccette in alto o in basso mi porta ai comandi precedentemente inseriti
        user_in.addKeyListener(new arrow());


        //completo il setup
        this.add(user_in, BorderLayout.SOUTH);
        this.add(scroll,BorderLayout.CENTER);
        //voglio che la finestra abbia sempre priorià sulle altre
        this.setAlwaysOnTop(true);
        this.setVisible(true);
        this.setPreferredSize(new Dimension(500,300));
        this.pack();
        //faccio partire il thread contenente il client
        new Thread(ct).start();


    }

    public class arrow implements KeyListener
    {

        @Override
        public void keyTyped(KeyEvent e) {

        }

        //mi è di interesse solamente la pressione delle freccette
        @Override
        public void keyPressed(KeyEvent e) {
            //prendo il codice corrispondente alla freccetta
            int code = e.getKeyCode();

            //se è stata premuta la freccia DONW
            if (code == KeyEvent.VK_DOWN) {
                //dato che mi sposto verso il basso voglio prendere il comando piu recente
                //verifico che l'indice sia minore della lunghezza della lista
                if(cmd_idx<all_cms.size()-1)
                {
                    // incremento l'indice e scrivo il comando corrispondente
                    cmd_idx++;
                    user_in.setText(all_cms.get(cmd_idx));

                }
                //altrimenti il comando più recente è vuoto
                else if(cmd_idx==all_cms.size()-1) {
                    user_in.setText("");
                }
            }
            //se invece la freccetta è in alto scrivo il comando meno recente con la stessa logica
            if (code == KeyEvent.VK_UP) {

                if(cmd_idx>0)
                {
                    cmd_idx--;

                    user_in.setText(all_cms.get(cmd_idx));

                }
                else if (cmd_idx==0)
                {
                    user_in.setText(all_cms.get(cmd_idx));

                }

            }
            //infine imposto il cursore alla fine del comando per velocizzare il processo
            user_in.setCaretPosition(user_in.getText().length());



        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }


    public void style_setup()
    {
        //a seconda dei messaggi che voglio printare sull'interfaccia uso diversi colori
        server_msg=new SimpleAttributeSet();
        StyleConstants.setBackground(server_msg, new Color(0,210,250,100));

        client_msg=new SimpleAttributeSet();
        StyleConstants.setBackground(client_msg, new Color(0,255,0,100));
        StyleConstants.setAlignment(client_msg,StyleConstants.ALIGN_RIGHT);//non funziona

        system_msg=new SimpleAttributeSet();
        StyleConstants.setBackground(system_msg, new Color(200,40,40,100));



    }



    public synchronized void write_to_client(String msg, SimpleAttributeSet style)
    {

        //questa funzione scrive i messaggi ricevuti dal server e digitati dall'user sulla gui
        try {
            sd.insertString(sd.getLength(),"\n"+msg+"\n",style );
            //faccio in modo che il focus sia sull'ultimo coamndo, ovvero che la posizione di scroll sia sempre la piu bassa
            scroll.validate();
            vertical.setValue(vertical.getMaximum());

        } catch (BadLocationException e) {
            e.printStackTrace();
        }


    }

    // Main function to star the Client graphic interface
    public static void main(String[] args) {
        new Client_GUI();
    }


}
