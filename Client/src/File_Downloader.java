import sun.misc.IOUtils;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by nicolo on 24/04/2017.
 */
public class File_Downloader implements Runnable{
    //consigio di guardare, contemporaneamente a questa classe, la classe FileSender dentro Server

    private final int port=3001;//la porta utilizzata dal file downloader
    private Socket s;
    public ObjectInputStream in;
    public ObjectOutputStream out;

    public File_Downloader_GUI main;
    public  Logger logger;

    private final File dir=new File(System.getProperty("user.dir")+"/Files");
    public volatile boolean cancel=false;// una variabile condivisa da piu thread che mi permette di cancellare l'operazione di download

    public File_Downloader(File_Downloader_GUI main,Logger logger)
    {
        this.main=main;
        this.logger=logger;
        try {
            this.s=new Socket("127.0.0.1",port);
            this.out=new ObjectOutputStream(s.getOutputStream());
            this.in=new ObjectInputStream(s.getInputStream());
            logger.log(Level.INFO,"file reader connected");
        } catch (IOException e) {
            logger.log(Level.WARNING,"cannot setup file reader",e);
            System.exit(0);
        }
    }


   public void join_files(File dir, List<File> files, String name)
   {
       //devo riunire tutti i file che ho scaricato
       File ris=new File(dir.getAbsolutePath()+"/"+name);
       try {
           //creo un output streamer che mi andrà a comporre il file completo
           FileOutputStream  out=new FileOutputStream(ris);
           Integer b;
           //setto il nuovo massimo della barra di progresso
           this.main.progress.setMaximum(files.size());

           // per ogni file della lista
           for(int i=0;i<files.size();i++)
           {
               //creo un nuovo reader dell'i-esimo file
               FileInputStream in=new FileInputStream(files.get(i));
               //lo leggo tutto scrivendo il risultato dentro ris
               while ((b=in.read())!=-1)
               {
                   out.write(b);
               }
               //aggiorno la progress bar
               this.main.progress.setValue(i);


           }
           logger.log(Level.INFO,"file join completed");

       } catch (FileNotFoundException e) {
           logger.log(Level.WARNING,"cannot join files",e);
           System.exit(0);       }
           catch (IOException e) {
           e.printStackTrace();
       }

   }

   public void kill()
   {
       try {
           out.close();
           in.close();
           s.close();
           logger.log(Level.INFO,"file reader closed");
       } catch (IOException e) {
           logger.log(Level.WARNING,"cannot close connection in file reader",e);
       }
   }

   public void download_completed(String name)
   {
       //rimuovo la progressbar
       this.main.remove(this.main.progress);
       //aggiungo un testo
       JTextArea complete=new JTextArea("Download completed\nFile "+name+", located in: "+dir.getAbsolutePath());
       complete.setEnabled(false);
       this.main.add(complete, BorderLayout.NORTH);
       //modifico il bottone cancel
       this.main.cancel.setText("Done");


   }
    @Override
    public void run() {

        logger.log(Level.INFO,"file reader running...");
        File f;
        Object resp;
        List<File> list=new ArrayList<>();

        try {
            //leggo il numero di file che il server ha intenzione di inviare, sono tutte pari dello
            // stesso file che dopo dovrò ricomporre
            int splits=in.readInt();
            //setto il massimo per la Jprogress bar al numero di split (Swing concurrency)
            main.progress.setMaximum(splits);


            //leggo il nome del file
            String name=in.readUTF();

            //per ogni file
            for(int i=0;i<splits;i++)
            {
                //leggo l'oggetto
                resp=in.readObject();
                if(File.class.isInstance(resp))
                {
                    //lo casto a file e lo aggiungo alla lista
                    f=(File) resp;
                    list.add(f);

                }
                //se l'oggetto ricevuto non è un file uccido il thread
                else
                {
                    logger.log(Level.WARNING,"object recived is not a file, obj: "+resp.getClass());
                    kill();

                }
                //per ogni file ricevuto aggiorno la progress bar
                main.progress.setValue(i);
                //se l'utente ha cancellato il download
                if(cancel) break;

            }

            if(!cancel)
            {
                //ricevuti tutti i file invia conferma al server
                out.writeBoolean(true);
                out.flush();
                //e chiamo la funzione per unire i file insime a quella di fine download
                join_files(dir,list,name);
                download_completed(name);
            }


        } catch (IOException e) {
            logger.log(Level.WARNING,"cannot read from file sender",e);
            System.exit(0);        }
            catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //uccido il thread
        kill();

    }
}
