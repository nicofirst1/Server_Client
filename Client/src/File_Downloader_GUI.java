import javax.swing.*;
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by nicolo on 25/04/2017.
 */
public class File_Downloader_GUI extends JFrame implements Runnable {
    //questa classe è un perfetto esempio di swing concurrecy

    //ho due varaibili swing volatitli che verranno modificate da un thread separato
    public volatile JProgressBar progress;
    public volatile JButton cancel;

    public Client main;
    public File_Downloader fd;
    public final Logger logger=Logger.getLogger("client.logger");


    public File_Downloader_GUI(Client main)
    {
        //inizzializzo le componenti swing
        this.main=main;
        this.cancel=new JButton("Cancel");
        this.progress=new JProgressBar();

        //aggiungo un listener tramite lambda
        this.cancel.addActionListener(action->
        {
            //se il bottone candel è pari a done chiudi la finestra
            if(cancel.getText().equals("Done"))
            {
                this.dispose();
            }
            //altrimenti notifica il thread File_Downloader che deve cancellare il download del file
            else
            {
                fd.cancel=true;
            }
        });

        this.add(progress, BorderLayout.NORTH);
        this.add(cancel, BorderLayout.SOUTH);

        this.setVisible(true);
        this.setAlwaysOnTop(true);
        this.pack();

    }


    @Override
    public void run() {
        //inizzializza l'fd e fai partire il thread
        logger.log(Level.INFO,"file downloader gui running...");
        this.fd=new File_Downloader(this,logger);
        new Thread(fd).start();



    }
}
