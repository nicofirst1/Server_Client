import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by nicolo on 29/04/2017.
 */
public class File_Writer_GUI extends JFrame implements Runnable {

    public Client main;
    public Socket s;
    public ObjectOutputStream out;
    public ObjectInputStream in;
    private final int port=3002;

    public final String file_dir=System.getProperty("user.dir")+"/Files/";
    public File file;
    public FileWriter fw;

    public final Logger log=Logger.getLogger("client.logger");

    public JTextArea file_text;
    public JButton submit;


    public File_Writer_GUI(Client main)
    {
        this.main=main;


        try {
            //inizzializzo le componenti socket
            this.s=new Socket(main.ip,port);
            this.out=new ObjectOutputStream(s.getOutputStream());
            this.in=new ObjectInputStream(s.getInputStream());

            log.log(Level.INFO,"file writer initialized!");
        } catch (IOException e) {
            log.log(Level.WARNING,"cannot initialize file writer",e);
            return;
        }

        //inizzializzo le componenti swing
        this.file_text=new JTextArea();
        this.file_text.setEnabled(true);
        this.submit=new JButton("submit");
        this.submit.setEnabled(false);

        //setto l'action listener per submit tramite funzione lambda
        this.submit.addActionListener(act->
        {
            try {
                //invio il testo al server
                out.writeObject(file_text.getText());
                out.flush();
                log.log(Level.INFO,"file sent");

                //se la risposta del server è false allora c'è stato un problema nel send del file
                if(!in.readBoolean())
                {
                    log.log(Level.INFO,"file not recived correclty");
                }
                else {
                    //altrimenti il file è stato correttamente spedito e notifico l'user
                    JOptionPane.showMessageDialog(null,"Your file has been saved");
                }

            } catch (IOException e) {
                log.log(Level.WARNING,"cannot write to file");
                JOptionPane.showMessageDialog(null,"Cannot write to file");
            }
            //alla fine del sent chiudo la connessione
            kill();


        });


        //muovo le componenti al posto giusto e inizzializzo la finestra
        this.add(file_text, BorderLayout.CENTER);
        this.add(submit,BorderLayout.SOUTH);
        //setto la chiusura default che non fa nulla
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        //per poi aggiungerne una custom per la chiusura controllata dei socket
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {

            }

            @Override
            public void windowClosed(WindowEvent e) {
                kill();

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
        this.setVisible(true);
        this.pack();


    }

    public int kill()
    {
        //cerco di chiudere gli streamer e il socket
        try
        {
            out.close();
            in.close();
            s.close();
            log.log(Level.INFO, "socket closed");
        } catch (IOException e) {
            //loggo eventuali errori
            log.log(Level.WARNING,"Cannot close socket");
            return 0;
        }

        //chiudo la finestra corrente e ritorno
        this.dispose();
        return 1;

    }

    @Override
    public void run() {

        try {

            //leggo se il file selezionato dall'utente esiste gia
            if(in.readBoolean())
            {
                JOptionPane.showMessageDialog(null,"Cannot modify existing file");

            }
            //se non esiste leggo il nome e lo creo
            else
            {
                file=new File(file_dir+in.readUTF());
                fw=new FileWriter(file);
                submit.setEnabled(true);
            }



        } catch (IOException e) {
            log.log(Level.WARNING,"cannot read from socket",e);
            kill();
        }

    }
}
