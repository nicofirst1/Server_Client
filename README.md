# What is
This is a simple java program for practise. 
It emulates a terminal window with a Server/Client connection.
# Topics:
### 1. Socket
- Opening/Connecting ServerSocket & socket
- Communication through **ObectInputStream** & **ObectOutputStream** 

### 2. Swing & Swing concurrency
- *Elementary*: JFrame, JTextPane, JTextField, JButton
- *Intermediate*: JScrollPane, JScrollBar, JProgressBar
- *Advanced*: StyledDocument,  SimpleAttributeSet, StyleConstants

### 3. Threads
- Runnable implementing classes
- Volatile variables
- Synchronized methods

### 4. Listeners
- Action listener
- Key Listener

### 5. Logging
- Custom handler
- Logging Object
- Levels

### 6. Classes & Methods
- Abstract classes & methods
- Extending custom abstract classes
- Overriding

### 7. Files
- Opening/creating/closing files
- Writing with **FileWriter**
- Reading with **BufferedReader**
- Splitting Files to be sent through socket
- Joining Files read from socket
- FileOutputStream, FileInputStream

### 8. Other
- Lambdas
- Error handling (try-catch)
- Interfaces
- Lists with diamond operator 
- Map (Hashtable)
- Runtime custom ctrl+c operation

# How to run 
To run you have to start two separated terminal windows, one for the Server and the other one for the Client.
In the Server one start **MAIN.java**, wait for the server to start and then run **ClientGui.java** in the other terminal window. 

# Info
Most of this project has comments written in **Italian**, but you can easily understand with some background knowledge and some patience.