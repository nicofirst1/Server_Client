import com.sun.corba.se.spi.activation.Server;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by nicolo on 23/04/2017.
 */
public class Client_thread implements Runnable {



    public Integer id;
    public SuperServer ss;

    //varibili in comune tra piu thread
    public volatile Boolean running;
    public volatile Map<String, Custom_server> sv_lst;

    //variabili per la connessione socket
    public Socket s;
    public ObjectOutputStream out;
    public ObjectInputStream in;

    public final Logger logger=Logger.getLogger("super.server.logger");

    //il client prende in ingresso 4 parametri:
    // Socket s: il socket associato al client
    // Integer id: l'id del client
    // Map sv_lst: stessa mappa presente nel superServer, e il super server stesso
    public Client_thread(Socket s, Integer id, Map<String, Custom_server> sv_lst, SuperServer ss)
    {
        this.s=s;
        this.id=id;
        this.sv_lst=sv_lst;
        this.ss=ss;


        //inizzializzo l'input e l'output
        try
        {
            this.out=new ObjectOutputStream(s.getOutputStream());
            this.in =new ObjectInputStream(s.getInputStream());

            logger.log(Level.INFO,"setup completed");
        }catch (IOException e)
        {
            logger.log(Level.WARNING,"setup failed",e);
            System.exit(0);
        }
    }

    public void kill()
    {

        try{
            //scrivo al client goodbey che scatena l'evento di chiusura dal lato client
            write_to_client("Goodbye",false);
            this.out.close();
            this.in.close();
            this.s.close();
            logger.log(Level.INFO,"connection closed");
        }catch (IOException e){
            logger.log(Level.WARNING,"cannot close connection ",e);
            running=false;
        }


    }

    //la funzione handshake è ricorrente all'interno del progetto, la sua funzione è quella di mandare l'id al client,
    // verificare che l'id sia stato ricevuto corretamente e inviare un ack
    public boolean handshake() {

        int resp;
        boolean correct=false;

        do {
            try {


                //inzio l'id al client
                out.writeInt(id);
                out.flush();

                //apsetto per leggere l'id che ha ricevuto
                resp = in.readInt();
                correct= resp==id;

                //invio il risultato della verifica al client
                out.writeBoolean(correct);
                out.flush();

            } catch (IOException e) {
                //se si verifica un qualche errore interrompo il running
                logger.log(Level.WARNING,"cannot close connection",e);
                running=false;
                break;
            }
        }while(!correct);//ripeto finche il client non rimanda indietro l'id corretto

        logger.log(Level.INFO,"handshake completed");
        return correct;

    }


    //eseguo l'ovverdie del metodo equals che verrà utilizzato nel custom server per verificare che un determinato
    // client sia gia connesso o meno al server stesso
    @Override
    public boolean equals(Object obj) {
        if(!Client_thread.class.isInstance(obj))
        {
            return false;
        }
        // due oggetti client sono uguali se il loro id sono identici
        Client_thread ct=(Client_thread) obj;
        if(ct.id!= this.id)
        {
            return false;
        }
        return true;


    }


    public void main_comand(String comand)
    {
        int resp;

        String [] cms=comand.split(":");
        String main_comand="";

        try
        {
            main_comand=cms[0].toLowerCase();
        }catch (IndexOutOfBoundsException e)
        {
            write_to_client("Incorrect formatting",true);
            return;
        }



        switch (main_comand) {
            //il client sta cercando di connettersi ad un server
            case "join":
                resp = ss.add_client(this, cms[1]);
                switch (resp) {
                    case -1: {
                        write_to_client("No server with that name", true);
                        break;
                    }
                    case 0: {
                        write_to_client("Client already subscribed to server " + cms[1], true);
                        break;
                    }
                    case 1: {
                        write_to_client("Connected to server: " + cms[1], true);
                        break;
                    }
                }
                break;
                //il client vuole chiudere la connessione
            case "close":
                kill();
                running = false;
                break;
                //il client vuole una lista di comandi da poter utilizzare
            case "help":
                write_to_client("Main server is your personal server\n" +
                        "commands avaiable now are:\njoin:server_name-> joins the server you have chosen\n" +
                        "close -> close comunication\n" +
                        "help -> this help page\n" +
                        "list -> show a list of possible servers", true);
                break;
                //il client vuole una lista di server a cui può connettersi
            case "list":
                String server_lst = "";
                //prendo una collezioni di custum server
                Collection<Custom_server> e = sv_lst.values();

                //per ogniumo prendo il nome
                for (Custom_server cs : e) {
                    server_lst += cs.name + "\n";
                }

                //e lo invio
                write_to_client(server_lst, true);
                break;
            default:
                //se non si verifica nessuno dei precedenti comandi è stato capito invio  questo
                write_to_client("cannot understand main command",true);
        }




    }

    public synchronized void write_to_client(Object msg, boolean listen_too)
    {
        try {

                //mando il messaggio al client ed eseguo il flush della pipe
                out.writeObject(msg);
                out.flush();

                //loggo i messaggi
            logger.log(Level.FINE,"\tSERVER: "+msg.toString()+"\n");

                //aspetto l'ack
                if(!in.readBoolean())
                {
                    logger.log(Level.WARNING,"error reading");

                }

        } catch (IOException e) {
            logger.log(Level.WARNING,"cannot write message: "+msg,e);
        }
        //mi metto in attesa della risposta del client
        if(listen_too) listen_from_client();
    }

    public boolean command(String cm)
    {

        // il comando è nel formato:
        // [A chi è indirizzato]:[il comando]:[eventuali argomenti aggiuntivi]


        if (cm.toLowerCase().contains("main")) {
            //il comando del cliet è indirizzato a quest thread, tolgo il main: e lo passo alla funzione
            main_comand(cm.replaceFirst("main:",""));

        }
        //altrimenti devo mandare il comando ad il server interessato (se esiste)
        else
        {
            String server_name;
            try
            {
                server_name=cm.split(":")[0];
            }catch (IndexOutOfBoundsException e)
            {
                write_to_client("incorrec format",true);
                return false;
            }

            //se il primo elemento del comando non è un nome di server, ottengo il valore null
            Custom_server sv=sv_lst.get(server_name);

            if(sv==null)
            {
                //non viene capito il comando
                write_to_client("incorrect server name",true);
            }
            //se invece il nome del server è corretto ma il client non è connesso al server
            else if(!sv.cl_lst.contains(this))
            {
                write_to_client("you are not subscribed to server "+server_name,true);
                return false;
            }

            else {
                Object resp;
                //prendo il server a cui è indirizzato il comando
                // passo il comando + argomento
                resp = sv.do_action(cm.replaceFirst(server_name + ":", ""), this);
                //invio la risposta se questa è stringa
                write_to_client(resp,true);

            }

        }

        return true;
    }



    //metodo synchronized perche chiamato da piu thread
    public synchronized void listen_from_client()
    {
        Object obj;

        try {

            // leggo la ripsota
            obj=in.readObject();

            //loggo la risposta con livello Fine, che verrà salvato dall'handler che si occupa di
            // salvare i messaggi tra client e server
            logger.log(Level.FINE,"\tCLIENT: "+obj.toString()+"\n");

                // invio l'ack al client
                out.writeBoolean(true);
                out.flush();

                //se il comando è string lo passo al metodo command
                if(String.class.isInstance(obj))
                {
                    command((String) obj);
                }
                else
                {
                    write_to_client("Non string message recived",true);
                    logger.log(Level.WARNING,"Recived non string object from client, obj: "+obj.getClass());
                }



        } catch (EOFException e) {
            logger.log(Level.WARNING,"has closed connection",e);
            running=false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            running=false;

        } catch (IOException e) {
            e.printStackTrace();
            running=false;

        }


    }

    @Override
    public void run() {
        //eseguo l'handshake e verifico che sia andato a buon fine
        running=handshake();


        //finche siamo in running ascolto il messaggio del client
        while (running)
        {

            listen_from_client();

        }
        //alla fine dell'esecuzione chiudo il socket
        kill();



    }
}


