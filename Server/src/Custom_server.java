import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolo on 23/04/2017.
 */
public abstract class Custom_server implements Runnable {
    //calsse astratta da cui devono derivare i custom server (math_server, file_server)

    public String name;//nome del server
    public  List<Client_thread> cl_lst;//lista di clienti connessi


    public Custom_server()
    {
        cl_lst=new ArrayList<>();
    }

    //funzione synchronized poiche chiamata da piu thread (Client_thread)
    public synchronized boolean add_client(Client_thread ct)
    {
        //se la lista contiene gia il client ritorno false
        // (Proprio per uqesto abbiamo modificato il metodo equals nella classe CLient_thread)
        if(cl_lst.contains(ct))
        {
            return false;
        }

        //altrimenti lo aggiungo
        cl_lst.add(ct);
        return true;


    }

    //metodo astratto che prende in ingresso un comando in formato stringa e il client che l'ha mandato, e ritorna
    // un oggetto da inviare al client. Il return è un oggetto poiche il client
    // puo ricevere sia stringhe che interi (per ora)
    public abstract Object do_action(String cm, Client_thread ct);


    //faccio l'override del metodo equals che verra usato nell'ss quando chiamo server_list.contains
    @Override
    public boolean equals(Object obj)
    {
        if(!Custom_server.class.isInstance(obj))
        {
            return false;
        }

        Custom_server cs=(Custom_server) obj;

        if(!cs.name.equals(this.name))
        {
            return false;
        }
        return true;
    }

    @Override
    public void run() {}
}
