import sun.rmi.runtime.Log;

import java.io.*;
import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by nicolo on 23/04/2017.
 */
public class File_Sender implements Runnable {

    public File f;
    private final int buff_size=1024;


    public Socket s;
    public ServerSocket listener;
    private final int port=3001;
    public ObjectInputStream in;
    public ObjectOutputStream out;

    public Logger logger;

    public File_Sender(File f)
    {
        this.f=f;
        logger=Logger.getLogger("super.server.logger");
        try {
            //attivo il listener
            this.listener=new ServerSocket(port);

            logger.log(Level.INFO,"file sender initialized");
        } catch (IOException e) {
            logger.log(Level.WARNING,"cannot setup file sender",e);

        }


    }


    //questa funzione permette di splittare un file in piu file per velocizzare l'upload
    public List<File> file_splitter(File f, int buff_size)
    {
        //prendo la lunghezza del file
        long file_size=f.length();
        //creo un buffer
        char [] buffer=new char[buff_size];
        //determino il numero di split
        int splits=(int)Math.ceil(file_size/buff_size);
        int to_write;

        //se il file è molto piccolo allora ho solo uno split
        if(splits==0) splits=1;

        //il risultato dello split sarà una lista di file
        List<File> ris=new ArrayList<>();
        File prov;//file temporaneo
        BufferedReader reader;
        BufferedWriter writer;
        int idx=0;

        try {
            //reader è costante, perche legge sempre da stesso file
            reader=new BufferedReader(new FileReader(f));

            //se to_write è -1 allora ho finito il file
            //leggo i dati dal file che volgio mandare e deciso quanto scrivere
            while ((to_write=reader.read(buffer))!=-1)
            {

                //creo un file provvisorio
                prov=File.createTempFile("file",idx+"");
                idx++;

                //creo uno writer per scrivere sul file provvisorio
                writer=new BufferedWriter(new FileWriter(prov));
                //scrivo sul file provvisorio
                writer.write(buffer,0,to_write);
                //lo aggiungo alla lista
                ris.add(prov);
                //eseguo il flush
                writer.flush();

            }
        } catch (IOException e) {
            logger.log(Level.WARNING,"cannot read file");
            System.exit(0);

        }
        return ris;
    }



    public void kill()
    {
        try
        {
            out.close();
            in.close();
            s.close();
            listener.close();
            logger.log(Level.INFO,"file sender closed");
        } catch (IOException e) {
            logger.log(Level.WARNING,"Cannot close connection");
        }
    }


    @Override
    public void run() {

        try {
            //inizzializzo gli streamer
            this.s=listener.accept();
            this.out=new ObjectOutputStream(s.getOutputStream());
            this.in=new ObjectInputStream(s.getInputStream());
            logger.log(Level.INFO,"client connected in file sender");

        } catch (IOException e) {
            logger.log(Level.WARNING,"cannot setup file sender");
            System.exit(0);

        }

        //ottengo la lista di file da mandare
        List<File> to_send=file_splitter(f,buff_size);

        try {
            //invio la lungheza della lista
            out.writeInt(to_send.size());
            out.flush();

            //invio il nome del file
            out.writeUTF(f.getName());
            out.flush();

            //invio tutti i file
            for(File f: to_send)
            {
                out.writeObject(f);
                out.flush();
            }

            //aspetto che il client abbia ricevuto tutti i file
            in.readBoolean();
            logger.log(Level.INFO,"all files sent");
        } catch (EOFException e) {
            logger.log(Level.WARNING,"Client has closed connection");

        } catch (IOException e) {
            e.printStackTrace();
        }

        kill();



    }

}
