import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by nicolo on 29/04/2017.
 */
public class File_reciver implements Runnable {

    public File to_write;
    public boolean exists;

    public ServerSocket listener;
    public int port=3002;
    public ObjectInputStream in;
    public ObjectOutputStream out;

    public Logger log;

    //prendo il path assoluto e aggiungo la dir Files
    public String dir=System.getProperty("user.dir")+"/Files/";

    public File_reciver(File to_write)
    {
        //prendo il file e ontrollo se esiste
        this.to_write=new File(dir+to_write.getName());
        exists=to_write.exists();
        log=Logger.getLogger("super.server.logger");

        try {
            //inizzializzo il listener
            listener=new ServerSocket(port);
            log.log(Level.INFO,"file reciver initialized");
        } catch (IOException e) {
            log.log(Level.WARNING,"cannot initialize file reciver",e);
        }

    }


    public void kill(){

        try {
            out.close();
            in.close();
            listener.close();
            log.log(Level.INFO,"socket closed");
        } catch (IOException e) {
            log.log(Level.WARNING,"cannot close socket");
        }
    }


    public void setup()
    {
        Socket client;

        try {
            //aspetto che il client si connetta e inizzializzo gli streamers
            client=listener.accept();
            out=new ObjectOutputStream(client.getOutputStream());
            in=new ObjectInputStream(client.getInputStream());

            //scrivo al client se il file esiste gia
            out.writeBoolean(exists);
            out.flush();

            log.log(Level.INFO,"Client connected, in/out initialized");


        } catch (IOException e) {
            log.log(Level.WARNING,"cannot accept client in file reciver",e);
            return;
        }

    }

    @Override
    public void run() {

        setup();

        //se esite devo inviare il testo
//        if(exists)
//        {
//            String line;
//            try {
//                BufferedReader br=new BufferedReader(new FileReader(to_write));
//                while((line=br.readLine())!=null)
//                {
//                    out.writeObject(line);
//                    out.flush();
//
//                }
//
//            } catch (FileNotFoundException e) {
//                log.log(Level.WARNING,"cannot find file",e);
//                kill();
//            } catch (IOException e) {
//                log.log(Level.WARNING,"cannot write file",e);
//                kill();
//            }
//        }

        //se il file non esiste
        if(!exists)
        {
            Object ob=null;

            try {

            //scrivo il nome del file al client
            out.writeUTF(to_write.getName());
            out.flush();

            //leggo il file di risposta
            ob=in.readObject();


            } catch (IOException e) {
                log.log(Level.WARNING,"cannot read input");
                kill();
                return;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            //se cio che ho ricevuto è il nomem di un file, lo printo sullo schermo (da finire)
            if(String.class.isInstance(ob))
            {
                System.out.println(ob);

                try {
                    //confermo che il file è stato ricevuto
                    out.writeBoolean(true);
                    out.flush();

                } catch (IOException e) {
                    log.log(Level.WARNING,"cannot sent confirmation");
                }

            }
            else
            {

                try {
                    log.log(Level.INFO,"Recived "+ob.getClass()+", insed of file");
                    out.writeBoolean(false);
                    out.flush();
                } catch (IOException e) {
                    log.log(Level.WARNING,"cannot write to client");
                    kill();
                }
            }

        }
        //termino il programma
        kill();



    }
}
