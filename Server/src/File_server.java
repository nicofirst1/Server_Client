import java.io.*;

/**
 * Created by nicolo on 23/04/2017.
 */
public class File_server extends Custom_server {
    //Questa classe è molto complicata,  leggete con attenzione

    public final File dir = new File(System.getProperty("user.dir")+"/Files");//con System.get...
    // ottengo il path assoluto alla directory corrente
    public  File[] all_files;

    public final String HELP="Welcome to file server, current avaiable commands:\n" +
            "list-> list files\n" +
            "print:x, with x:\n" +
            "\tpath-> print files path\n" +
            "\tpreview,file_name, lines-> print first 'lines' lines fr the 'file_name' path (may " +
            "take some time, expecially with large files)\n" +
            "download:file_name -> download 'filen-name'\n" +
            "write:file_name -> write file";



    public File_server()
    {
        //inizzializzo il nome del server e la directory in cui sono salvati i file
        this.name="file";
        all_files=dir.listFiles();
    }

    public String print_cmd(String args)
    {
        String ris="";
        //se gli argomenti sono vuoti ritorno un errore
        if(args.equals(""))
        {
            ris="incorrect arguments";
        }
        //se, invece, gli argomenti contengono la parola path,, ritorno il path dove sono situati i file
        else if(args.toLowerCase().contains("path"))
        {
            ris=dir.getAbsolutePath();
        }

        //preview invece ritorna un tot di righe del file scelto
        else if(args.toLowerCase().contains("preview"))
        {
            try {
                String file_name=args.split(",")[1];
                String lines=args.split(",")[2];
                int lines_i=Integer.parseInt(lines);

                for(File f:all_files)
                {
                    String fn=f.getName().toLowerCase();
                    if(fn.toLowerCase().contains(file_name))
                    {
                        BufferedReader reader=new BufferedReader(new FileReader(f));
                        int idx=0;
                        String line;

                        while ((line=reader.readLine())!=null || idx>lines_i)
                        {
                            ris+=line;
                        }
                    }
                }

            }catch (IndexOutOfBoundsException e) {
                ris="Comand format error";
            }catch (NumberFormatException e) {
                ris="lines are not a vaid number";
            } catch (FileNotFoundException e) {
                ris="File not found";
            } catch (IOException e) {
                ris+="Error reading line";
            }
        }
        return ris;

    }



    @Override
    public Object do_action(String cm, Client_thread ct) {
        //il comando ora si compone solo di: [comando]:[argomenti opzionali]

        String comand=cm.split(":")[0].toLowerCase();
        String args="";
        try
        {
            //provo a vedere se ci sono argomenti
            args=cm.split(":")[1].toLowerCase();

        }catch (IndexOutOfBoundsException e)
        {}

        String ris="";


        switch (comand)
        {
            //se il comando è help invio la lista di comandi disponibili
            case "help": ris=HELP;
            break;

            //se il comando è list scrivo tutti i file presenti nella cartella Files
            case "list":
                for(File f: all_files)
                {
                    ris+=f.getName()+"\n";
                }
                break;


                // se è print posso avere piu opzioni
            case "print": ris=print_cmd(args);
            break;

                //download permette di scaricare il file
            case "download":
                File f=null;

                //cerco il file che l'utente vuole scaricarare
                for(File f1:all_files)
                {
                    String fn=f1.getName().toLowerCase();
                    if(fn.toLowerCase().contains(args.toLowerCase()))
                    {
                        f=f1;
                        break;
                    }
                }
                //se il file non è stato trovato ritorno errore
                if(f==null)
                {
                    return "File not found";
                }

                // scrivo al client che sta per scaricare un file, con un intero
                ct.write_to_client(1,false);

                //inizzializzo il file sender e lo faccio partire in un nuovo thread
                new Thread(new File_Sender(f)).start();

                //dico al client che puo far partire il suo file downloader
                ct.write_to_client(true,false);
                break;


                //se il comando è write allora l'utente vuole scrivere un file
            case "write":
                //controllo che ci siano gli argomenti giusti
                if(args.equals("")){
                    return "Invalid argument";
                }

                //creo il file che l'utente vuole scrivere e faccio partire il thread con il servizio File_reciver
                File write_file=new File(dir+args);
                new Thread(new File_reciver(write_file)).start();

                //notifico il client che l'utente vuole scrivere un file
                return 2;

            default: ris = "File server: command not understood";



        }


        return ris;

    }


}
