import java.util.*;

/**
 * Created by nicolo on 23/04/2017.
 */
public class MAIN {

    public static void main(String[] args) {
        //creo una hashtable (dizionario python) per ogni custom server
        Map<String, Custom_server> cs_lst=new Hashtable<>();

        //inizzializzo i vari server
        Custom_server math=new Math_server();
        Custom_server file=new File_server();

        //li aggiungo alla lista
        cs_lst.put(math.name,math);
        cs_lst.put(file.name,file);

        //inizzializzo e faccio partire il superServer
        SuperServer ss=new SuperServer(cs_lst);
        ss.start();
    }
}
