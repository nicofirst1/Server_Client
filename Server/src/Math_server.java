/**
 * Created by nicolo on 23/04/2017.
 */
public class Math_server extends Custom_server {

    public Math_server()
    {
        super();
        this.name="math";
    }

    @Override
    public String do_action(String cm, Client_thread ct) {

        String [] cms=cm.split(":");
        String ris="";

        if(cms[0].toLowerCase().equals("add"))
        {
            String [] numbs=cms[1].toLowerCase().split(",");
            int a=Integer.parseInt(numbs[0]);
            int b=Integer.parseInt(numbs[1]);
            ris= a+b+"";

        }
        else if(cms[0].toLowerCase().equals("help"))
        {
            ris="Welcome to math server, list of avaiable comands:\n" +
                    "add:num,num\n" +
                    "sub:num,num\n";
        }
        else if(cms[0].toLowerCase().equals("sub"))
        {
            String [] numbs=cms[1].toLowerCase().split(",");
            int a=Integer.parseInt(numbs[0]);
            int b=Integer.parseInt(numbs[1]);
            ris= a-b+"";

        }

        return ris;

    }
}
