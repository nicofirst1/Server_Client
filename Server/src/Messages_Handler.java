import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Created by nicolo on 29/04/2017.
 */
public class Messages_Handler extends Handler {
    //questa classe è utilizzata per loggare i messaggi con priorità FINE nel file masgs_loggs

    public File msgs_file;
    public FileWriter writer;

    public Messages_Handler(File msgs_file)
    {
        this.msgs_file=msgs_file;

        try {
            //inizzializzo il file printer per scrivere nel file
            writer=new FileWriter(msgs_file);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void publish(LogRecord record) {
        //ogni volta che viene chiamato logger.log, questa funzione parte,
        // prima mi assicuro che il livello sia FINE, altrimenti sono messaggi indirizzati per l'altro file di log

        if(record.getLevel().equals(Level.FINE))
        {
            //poi scrivo il messaggio sul file in modalità appenda
            String to_log=record.getMessage()+"\n";
            try {
                writer.append(to_log);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void flush() {
        try {
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void close() throws SecurityException {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
