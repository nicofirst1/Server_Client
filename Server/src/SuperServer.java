import java.net.ServerSocket;
import java.util.List;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by nicolo on 23/04/2017.
 */
public class SuperServer {

    //parametri per la connessione socket
    private final int port=3000;
    public ServerSocket listener;

    //variabili in comune tra piu thread
    public volatile boolean running;
    public volatile List<Client_thread> cl_lst;
    public volatile Map<String, Custom_server> sv_lst;

    //variabili per il logger
    public Logger logger;
    public File info_log_file;
    public File msgs_log_file;

    //il costruttore prende in ingresso una Mappa con chiavi in formato stringa e valori di tipo Custom Server
    public SuperServer(Map<String, Custom_server> sv_lst)
    {
        this.sv_lst=sv_lst;

        //inizzializzo il logger
        logger=Logger.getLogger("super.server.logger");
        logger.setLevel(Level.ALL);

        //inizzializzo i file di log, li creo se non esistono
        info_log_file=new File(System.getProperty("user.dir")+"/loggs/info_logs.txt");
        msgs_log_file=new File(System.getProperty("user.dir")+"/loggs/messages_log.txt");
        if(!info_log_file.exists()) logger.log(Level.INFO, "created info_log file");
        if(!msgs_log_file.exists()) logger.log(Level.INFO, "created messages_log file");

        //inizzializzo l'handler per il logger
        logger_setup();


        try {

            //inizzializzo il listener
            listener=new ServerSocket(port);
            cl_lst=new ArrayList<>();

            //aggiungo un'azione di uscita quando viene rilevata l'interruzione di esecuzione con ctr+c
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    //per ogni cliente connesso, chiudo la connessione
                    for(Client_thread ct: cl_lst)
                    {
                        ct.kill();
                    }
                }
            });


        } catch (IOException e) {
            //se avviene un errore da subito termino l'esecuzione del programma
            logger.log(Level.WARNING,"Cannot initialize super server",e);
            System.exit(0);
        }

    }


    public void logger_setup()
    {
        try {
            //questo è l'handler per le info e  i warning
            FileHandler info_handler=new FileHandler(info_log_file.getAbsolutePath());
            info_handler.setLevel(Level.INFO);// scrive solo i messaggi con livello >=Info
            //uso un formatter normale per il txt
            info_handler.setFormatter(new SimpleFormatter());
            logger.addHandler(info_handler);

            //aggiungo un handler speciale per i log riguardanti i messaggi tra client e server
            logger.addHandler(new Messages_Handler(msgs_log_file));

        } catch (IOException e) {
            logger.log(Level.WARNING,"cannot setup handlers",e);

        }


    }



    public void start()
    {
        logger.log(Level.INFO,"Super server running");
        running=true;
        Socket s;
        Client_thread ct;
        int idx=0;

        //finche sono in running
        while (running)
        {
            try {
                //accetto il client
                s=listener.accept();
                //creo una nuova classe client
                // aggiorno l'indice
                // aggiungo il client alla lista
                ct=new Client_thread(s, idx, sv_lst, this);
                idx++;
                cl_lst.add(ct);

                //faccio partire un nuovo thread per il client
                new Thread(ct).start();


            } catch (IOException e) {
                logger.log(Level.WARNING, "cannot listen on ss",e);
                running=false;
            }
        }

        //se arrivo qui allora l'ss è stato interrotto
        //  chiudo tutti i client
        for(Client_thread ct1: cl_lst)
        {
            ct1.kill();
        }



        try {
            //infine chiudo il ServerSocket
            listener.close();
            logger.log(Level.INFO,"listener closed");
        } catch (IOException e) {
            logger.log(Level.WARNING,"Cannot close listener",e);
        }


    }


    //metodo utilizzato da piu thread, quindi deve essere synchronized per garantire l'esecuzione concorrenziale,
    // la funzione prende in input un nome corrispondente al server a cui il client si vuole collegare e il client stesso
    public synchronized int add_client(Client_thread ct, String name)
    {

        if(sv_lst.get(name)==null)
        {
            //ritorno -1 se non esiste il server con quel nome
            return -1;
        }
        //prendo il server in questione
        Custom_server sv=sv_lst.get(name);


        if(!sv.add_client(ct)) //aggiunge il client se non dovesse essere presente nella lista
        {
            // se il server contiene gia il client ritorno 0
            return 0;
        }

        return 1;


    }


}
